<?php

namespace App\Tests;

use App\Entity\Equipment;
use PHPUnit\Framework\TestCase;
use Doctrine\Persistence\ManagerRegistry;

class EquipmentTest extends TestCase
{
    public function testEquipment(): void
    {
        // $entityManager = $doctrine->getManager();

        $equipment = new Equipment();
        $equipment->setName('Ecran');
        $equipment->setNumber('0000001');
        $equipment->setDescription('Une petite description');
        $equipment->setCreatedAt(new \DateTimeImmutable());
        $equipment->setUpdatedAt(new \DateTimeImmutable());

        // $entityManager->persist($equipment);

        // $entityManager->flush();
        $this->assertSame('Ecran', $equipment->getName());
    }
}
