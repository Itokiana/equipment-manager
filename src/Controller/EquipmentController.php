<?php

namespace App\Controller;

use App\Entity\Equipment;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Serializer\SerializerInterface;

class EquipmentController extends AbstractController
{
    #[Route('/api/equipments', name: 'add_equipment', methods: ['POST'])]
    public function addEquipment(Request $request, EntityManagerInterface $em): JsonResponse
    {
        $data = json_decode($request->getContent(), true);

        $equipment = new Equipment();
        $equipment->setName($data['name']);
        $equipment->setCategory($data['category']);
        $equipment->setNumber($data['number']);
        $equipment->setDescription($data['description']);
        $equipment->setCreatedAt(new \DateTimeImmutable());
        $equipment->setUpdatedAt(new \DateTimeImmutable());

        $em->persist($equipment);
        $em->flush();

        return new JsonResponse('Equipment added successfully');
    }

    #[Route('/api/equipments/{id}', name: 'update_equipment', methods: ['PUT'])]
    public function updateEquipment(int $id, Request $request, EntityManagerInterface $em): JsonResponse
    {
        $data = json_decode($request->getContent(), true);

        $equipment = $em->getRepository(Equipment::class)->find($id);

        if (!$equipment) {
            throw $this->createNotFoundException(
                'No equipment found for id '.$id
            );
        }

        $equipment->setName($data['name']);
        $equipment->setCategory($data['category']);
        $equipment->setNumber($data['number']);
        $equipment->setDescription($data['description']);
        $equipment->setUpdatedAt(new \DateTimeImmutable());

        $em->flush();

        return new JsonResponse([
          'message' => 'Equipment updated successfully'
        ]);
    }

    #[Route('/api/equipments/{id}', name: 'delete_equipment', methods: ['DELETE'])]
    public function deleteEquipment(int $id, EntityManagerInterface $em): JsonResponse
    {
        $equipment = $em->getRepository(Equipment::class)->find($id);

        if (!$equipment) {
            throw $this->createNotFoundException(
                'No equipment found for id '.$id
            );
        }

        $em->remove($equipment);
        $em->flush();

        return new JsonResponse([
          'message' => 'Equipment deleted successfully'
        ]);
    }

    #[Route('/api/equipments/{filter?}', name: 'get_equipments', methods: ['GET'])]
    public function getEquipments(EntityManagerInterface $em, SerializerInterface $serializer, $filter = null): JsonResponse
    {
        $repository = $em->getRepository(Equipment::class);

        if ($filter) {
          $query = $repository->createQueryBuilder('e')
              ->where('e.name LIKE :name')
              ->setParameter('name', '%' . $filter . '%')
              ->getQuery();

          $equipments = $query->getResult();
        } else {
            $equipments = $repository->findAll();
        }

        $data = $serializer->serialize($equipments, 'json');
        return new JsonResponse($data, Response::HTTP_OK, [], true);
    }
}
