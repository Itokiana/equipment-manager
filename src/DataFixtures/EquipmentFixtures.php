<?php

namespace App\DataFixtures;

use App\Entity\Equipment;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;

class EquipmentFixtures extends Fixture
{
    public function load(ObjectManager $manager): void
    {
        $equipment = new Equipment();
        $equipment->setName('Ecran');
        $equipment->setNumber('0000001');
        $equipment->setDescription('Une petite description');
        $equipment->setCreatedAt(new \DateTimeImmutable());
        $equipment->setUpdatedAt(new \DateTimeImmutable());

        $manager->persist($equipment);

        $equipment = new Equipment();
        $equipment->setName('Clavier');
        $equipment->setNumber('0000001');
        $equipment->setDescription('Une petite description');
        $equipment->setCreatedAt(new \DateTimeImmutable());
        $equipment->setUpdatedAt(new \DateTimeImmutable());

        $manager->persist($equipment);

        $equipment = new Equipment();
        $equipment->setName('Souris');
        $equipment->setNumber('0000001');
        $equipment->setDescription('Une petite description');
        $equipment->setCreatedAt(new \DateTimeImmutable());
        $equipment->setUpdatedAt(new \DateTimeImmutable());

        $manager->persist($equipment);

        $manager->flush();
    }
}
