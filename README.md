le projet est un logiciel back-end de gestion de matériel confié aux employés d'une entreprise
dans le cadre de leur fonction. Un équipement regroupe tout matériel nécessaire à un salarié
pour exercer ses fonctions. Pour un développeur il s'agirait d'un ordinateur, d'un écran et des
souris et claviers.

## Demarrage rapide du projet

- Installer les composants:

```bash
composer install
```

- Lancer les migrations:

```bash
symfony console doctrine:migrations:migrate
```

- Enregistrer les donnees de test:

```bash
symfony console doctrine:fixtures:load
```

- Lancer le serveur:

```bash
symfony console server:start
```

- Les differentes routes pour tester:

```
# 1. Créer une ressource API pour ajouter un équipement ;

POST   /api/equipements


# 2. Créer une ressource API pour modifier un équipement que l'on a créé ;

PUT    /api/equipements/:id

# 3. Créer une ressource API pour supprimer un équipement que l'on a créé ;

DELETE /api/equipements/:id


4. Créer une ressource API pour afficher tous les équipements non supprimés sous
forme d'un tableau avec filtres.

GET    /api/equipements/:filter?
```

## Lancement de quelques tests unitaire:

```bash
php vendor/bin/phpunit tests/
```

## Details techniques:

Voici la liste techniques:

- Symfony 5.4
- PHP 8.2.1
- MySQL

